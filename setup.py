from setuptools import setup
from pathlib import Path
requirements = Path('requirements').read_text().splitlines()

setup(
    name="aurorapy",
    version="0.2.7",
    description='Python implementation of Aurora Protocol',
    author="E.Va Energie Valsabbia",
    author_email="it@energievalsabbia.it",
    maintainer='Claudio Catterina',
    maintainer_email='ccatterina@energievalsabbia.it',
    license='MIT',
    packages=['aurorapy'],
    url='https://gitlab.com/energievalsabbia/aurorapy',
    install_requires=[requirements],
    tests_require=['pytest'],
    classifiers=[
        # 'Development Status :: 1 - Planning',
        # 'Development Status :: 2 - Pre-Alpha',
        # 'Development Status :: 3 - Alpha',
        'Development Status :: 4 - Beta',
        # 'Development Status :: 6 - Mature',
        # 'Development Status :: 7 - Inactive',
        'Intended Audience :: Developers',
        'Operating System :: OS Independent',
        'License :: OSI Approved :: MIT License',
        'Environment :: Console',
        'Programming Language :: Python',
    ]
)
