import socket
import select
import binascii

from aurorapy.client import AuroraBaseClient
from aurorapy.defaults import Defaults
from aurorapy.client import AuroraError

from aurorapy.client import logger
class AuroraTCPClient(AuroraBaseClient):
    """
    Implementation of Aurora Power-One inverters protocol over TCP
    with the serial line converted to Ethernet.

    Arguments:
        - ip: IP address of the inverter/ethernet-converter.
        - port: TCP Port of the inverter/ethernet-converter.
        - address: Serial line address of the inverter.
    """

    def __init__(self, ip, port, address, timeout=Defaults.TIMEOUT):
        self.ip = ip
        self.port = port
        self.address = address
        self.timeout = timeout
        self.s: socket.socket | None = None

    def connect(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.s.connect((self.ip, self.port))
        except socket.error as e:
            self.s = None
            raise AuroraError(str(e))

    def close(self):
        if self.s is not None:
            self.s.close()
        else:
            raise AuroraError("Socket is not open")

    def send_and_recv(self, request):
        """
        Sends a request message and waits for the response.

        Arguments:
            request: Request message. [bytearray]

        Returns:
            Response message [bytearray]
        """
        if not self.s:
            raise AuroraError("You must connect client before launch a command")

        try:
            # Empty the socket buffer before send request and receive response
            # this is made to prevent receipt of noise in the response packet.
            ready = select.select([self.s], [], [], 0.1)
            if ready[0]:
                noise = self.s.recv(4096)
                logger.warning('Found noises on the socket buffer: %s' % (binascii.hexlify(noise)))

            self.s.send(request)
            self.s.setblocking(False)
            response = b''
            while(len(response) < 8):
                ready = select.select([self.s], [], [], self.timeout)
                if ready[0]:
                    response += self.s.recv(1024)
                else:
                    raise AuroraError("Reading Timeout")
        except socket.error as e:
            raise AuroraError("Socket Error: " + str(e))

        return bytearray(response)
