import serial
from serial import SerialException

from aurorapy.client import AuroraBaseClient, AuroraError, AuroraTimeoutError
from aurorapy.defaults import Defaults

class AuroraSerialClient(AuroraBaseClient):
    """
    Implementation of Aurora Power-One inverters protocol over serial line.

    Arguments:
        - serial_line: Serial line. [serial.Serial]
        - address: Serial line address of the inverter. [int] (default: 2)
        - timeout: Timeout of the serial line. [int] (default: 5)
        - tries: Number of tries to send a message. [int] (default: 3)
    """

    def __init__(
            self,
            serial_line: serial.Serial,
            address: int = Defaults.ADDRESS,
            timeout: int = Defaults.TIMEOUT,
            tries: int = Defaults.TRIES,
            ):

        self.address = address
        self.serline = serial_line
        self.timeout = timeout
        self.tries = tries

    @classmethod
    def from_connection_parameters(
        cls,
        address: int = 2,
        port: str = Defaults.PORT,
        baudrate: int = Defaults.BAUDRATE,
        parity: str = Defaults.PARITY,
        stop_bits: int = Defaults.STOP_BITS,
        data_bits: int = Defaults.DATA_BITS,
        timeout: int = Defaults.TIMEOUT,
        tries: int = Defaults.TRIES,
    ) -> 'AuroraSerialClient':
        """
        Creates a new AuroraSerialClient from connection parameters.

        Arguments:
            - address: Serial line address of the inverter. [int] (default: 2)
            - port: Serial port. [string] (default: '/dev/ttyUSB0')
            - baudrate: Baudrate of the serial line. [int] (default: 19200)
            - parity: Parity of the serial line. ('E'/'O'/'N') [char] (default: 'N')
            - stop_bits: Stop bits of the serial line. [int] (default: 1)
            - data_bits: Data bits of the serial line. [int] (default: 8)
            (for parity, stop_bits and data_bits, serial module has some constants.)
            - timeout: Timeout of the serial line. [int] (default: 5)
            - tries: Number of tries to send a message. [int] (default: 3)

        Returns:
            A new AuroraSerialClient instance.
        """

        serial_line = serial.Serial(
            port=port,
            baudrate=baudrate,
            parity=parity,
            stopbits=stop_bits,
            bytesize=data_bits,
            timeout=timeout
        )
        return cls(
            serial_line=serial_line,
            address=address,
            timeout=timeout,
            tries=tries
        )

    def connect(self):
        if self.serline.is_open:
            pass
        else:
            try:
                self.serline.open()
            except SerialException as e:
                raise AuroraError(str(e))

    def close(self):
        if not self.serline.is_open:
            pass
        else:
            try:
                self.serline.close()
            except SerialException as e:
                raise AuroraError(str(e))

    def send_and_recv(self, request):
        """
        Sends a request message and waits for the response.

        Arguments:
            request: Request message. [bytearray]

        Returns:
            Response message [bytearray]
        """
        if not self.serline.is_open:
            raise AuroraError("Serial line closed")

        self.serline.reset_input_buffer()
        self.serline.reset_output_buffer()

        response = b''
        tries = self.tries
        self.serline.write(request)

        while(len(response) < 8 and tries >= 0):
            tries = tries - 1
            try:
                response += self.serline.readline(8 - len(response))
            except SerialException as e:
                self.serline.close()
                raise AuroraError(str(e))

        if tries == -1 and len(response) < 8:
            raise AuroraTimeoutError('No response after %d tries' % self.tries)

        return bytearray(response)