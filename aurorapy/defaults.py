"""
Application default settings
"""

import serial
class Defaults(object):
    ADDRESS: int = 2
    BAUDRATE: int = 19200
    PARITY: str = 'N'
    STOP_BITS: int = 1
    DATA_BITS: int = 8
    TIMEOUT: int = 5  # seconds
    TRIES: int = 3
    PORT: str = '/dev/ttyUSB0'
